/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */


package minicp.engine.constraints;

import minicp.cp.Factory;
import minicp.engine.core.AbstractConstraint;
import minicp.engine.core.BoolVar;
import minicp.engine.core.IntVar;
import minicp.util.exception.InconsistencyException;
import minicp.util.exception.NotImplementedException;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.stream.IntStream;

import static minicp.cp.Factory.*;

/**
 * Disjunctive Scheduling Constraint:
 * Any two pairs of activities cannot overlap in time.
 */
public class Disjunctive extends AbstractConstraint {

    private final IntVar[] start;
    private final int[] duration;
    private final IntVar[] end;
    private final boolean postMirror;
    private final ThetaTree TT;



    /**
     * Creates a disjunctive constraint that enforces
     * that for any two pair i,j of activities we have
     * {@code start[i]+duration[i] <= start[j] or start[j]+duration[j] <= start[i]}.
     *
     * @param start the start times of the activities
     * @param duration the durations of the activities
     */
    public Disjunctive(IntVar[] start, int[] duration) {
        this(start, duration, true);
    }


    private Disjunctive(IntVar[] start, int[] duration, boolean postMirror) {
        super(start[0].getSolver());
        this.start = start;
        this.duration = duration;
        this.end = Factory.makeIntVarArray(start.length, i -> plus(start[i], duration[i]));
        this.postMirror = postMirror;
        this.TT = new ThetaTree(start.length);

    }


    @Override
    public void post() {

        int[] demands = new int[start.length];
        for (int i = 0; i < start.length; i++) {
            demands[i] = 1;
        }
        getSolver().post(new Cumulative(start, duration, demands, 1), false);


        // TODO 1: replace by  posting  binary decomposition using IsLessOrEqualVar
        for(int i = 0; i < start.length-1; i++){
            for(int j = i+1; j < start.length; j++){
                BoolVar bij = Factory.makeBoolVar(getSolver());
                BoolVar bji = Factory.makeBoolVar(getSolver());
                getSolver().post(new IsLessOrEqualVar(bij,end[i],start[j]));
                getSolver().post(new IsLessOrEqualVar(bji,end[j],start[i]));
                getSolver().post(new NotEqual(bij,bji));
            }
        }
        // TODO 2: add the mirror filtering as done in the Cumulative Constraint
        if (postMirror) {
            IntVar[] startMirror = Factory.makeIntVarArray(start.length, i -> minus(end[i]));
            getSolver().post(new Disjunctive(startMirror, duration, false), false);
        }
        propagate();

    }

    @Override
    public void propagate() {
        // TODO 6 (optional, for a bonus): implement the Lambda-Theta tree and implement the Edge-Finding        overLoadChecker();

        boolean fixed = false;
        while (!fixed) {
            overLoadChecker();
            fixed = !detectablePrecedence() && !notLast();
        }

    }


    private void overLoadChecker() {

        int[] Tlct = IntStream.range(0,end.length).boxed()
                .sorted(Comparator.comparingInt(x -> end[x].max() ))
                .mapToInt(x -> x).toArray();
        int[] Test = IntStream.range(0,end.length).boxed()
                .sorted(Comparator.comparingInt(x -> start[x].min() ))
                .mapToInt(x -> x).toArray();
        int[] pos = new int[Test.length];
        for(int i = 0; i < Test.length; i++){
            pos[Test[i]] = i;
        }

         TT.reset();

         for(int i : Tlct){
             TT.insert(pos[i], end[i].min(), duration[i]);
             if(TT.getECT() > end[i].max()){
                 throw new InconsistencyException();
             }
         }
    }

    /**
     * @return true if one domain was changed by the detectable precedence algo
     */
    private boolean detectablePrecedence() {
        boolean detection = false;

        int[] Tlst = IntStream.range(0, start.length)
                .boxed().sorted(Comparator.comparingInt(x -> start[x].max() ))
                .mapToInt(ele -> ele).toArray();
        int[] Test = IntStream.range(0,end.length).boxed()
                .sorted(Comparator.comparingInt(x -> start[x].min() ))
                .mapToInt(x -> x).toArray();
        int[] Tect = IntStream.range(0, start.length)
                .boxed().sorted(Comparator.comparingInt(x -> end[x].min() ))
                .mapToInt(ele -> ele).toArray();
        int[] pos = new int[Test.length];
        boolean[] wasInserted = new boolean[pos.length];
        for(int i = 0; i < Test.length; i++){
            pos[Test[i]] = i;
        }

        int J = 0;
        int j = Tlst[J];
        int[] est = new int[end.length];
        for(int i = 0; i < end.length; i++){
            est[i] = start[i].min();
        }
        boolean stop = true;

        TT.reset();

        for(int i : Tect) {

            while (end[i].min() > start[Tlst[j]].max() && stop) {
                TT.insert(pos[j], end[j].min(), duration[j]);
                wasInserted[j] = true;
                if (J < Tlst.length-1) {
                    j = Tlst[J++];
                }
                else stop = false;
            }
            if(wasInserted[i]) TT.remove(pos[i]);
            if (start[i].min() < TT.getECT()) {
                est[i] = TT.getECT();
                detection = true;
            }
            if(wasInserted[i]) TT.insert(pos[i], end[Tect[i]].min(), duration[Tect[i]]);
        }

        for(int i = 0; i < end.length; i++){
            start[i].removeBelow(est[i]);
            end[i].removeBelow(est[i]+duration[i]);
        }

        return detection;
    }

    /**
     * @return true if one domain was changed by the not-last algo
     */
    private boolean notLast() {
        boolean detection = false;

        int[] Tlst = IntStream.range(0,end.length).boxed()
                .sorted(Comparator.comparingInt(x -> start[x].max() ))
                .mapToInt(x -> x).toArray();
        int[] Tlct = IntStream.range(0,end.length).boxed()
                .sorted(Comparator.comparingInt(x -> end[x].max() ))
                .mapToInt(x -> x).toArray();
        int[] Test = IntStream.range(0,end.length).boxed()
                .sorted(Comparator.comparingInt(x -> start[x].min() ))
                .mapToInt(x -> x).toArray();
        int[] pos = new int[Test.length];
        boolean[] wasInserted = new boolean[pos.length];
        for(int i = 0; i < Test.length; i++){
            pos[Test[i]] = i;
        }

        int K = 0;
        int k = Tlst[K];
        int j = -1;
        int[] lct = new int[end.length];
        for(int i = 0; i < end.length; i++){
            lct[i] = end[i].max();
        }
        boolean stop = true;

        TT.reset();

        for(int i : Tlct){
            while((end[i].max() > start[k].max()) && stop){
                TT.insert(pos[k], end[k].min(), duration[k]);
                wasInserted[k] = true;
                j = k;
                if(K < Tlst.length-1) {
                    K++;
                    k = Tlst[K];
                }
                else stop = false;
            }
            if(j!=-1) {
                if (wasInserted[i]) TT.remove(pos[i]);
                if ((TT.getECT() > start[i].max()) && (end[i].max() > start[j].max())) {
                    detection = true;
                    lct[i] = start[j].max();
                }
                if (wasInserted[i]) TT.insert(pos[i], end[i].min(), duration[i]);
            }
        }

        for(int i = 0; i < end.length; i++){
            start[i].removeAbove(lct[i]-duration[i]);
            end[i].removeAbove(lct[i]);
        }

        return detection;
    }


}
