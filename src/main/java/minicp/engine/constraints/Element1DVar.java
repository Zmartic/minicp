/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */

package minicp.engine.constraints;

import minicp.engine.core.AbstractConstraint;
import minicp.engine.core.IntVar;
import minicp.engine.core.SparseSetDomain;
import minicp.state.StateInt;
import minicp.util.exception.NotImplementedException;

import java.util.Arrays;
import java.util.HashMap;

public class Element1DVar extends AbstractConstraint {

    private final IntVar[] array;
    private final IntVar y;
    private final IntVar z;
    private final StateInt[] tSupp;
    HashMap<Integer,StateInt> zSupp;

    

    public Element1DVar(IntVar[] array, IntVar y, IntVar z) {
        super(y.getSolver());
        this.array = array;
        this.y = y;
        this.z = z;
        y.removeBelow(0);
        y.removeAbove(array.length - 1);
        int[] index = new int[y.size()];
        y.fillArray(index);
        boolean findSupp;

        this.tSupp = new StateInt[array.length];
        for(int i : index){
            int[] tmp = new int[array[i].size()];
            array[i].fillArray(tmp);
            findSupp = false;
            for(int v : tmp){
                if(z.contains(v)){
                    tSupp[i] = y.getSolver().getStateManager().makeStateInt(v);
                    findSupp = true;
                    break;
                }
            }
            if(!findSupp) y.remove(i);
        }

        this.zSupp = new HashMap<>(z.size());
        int[] tmp = new int[z.size()];
        z.fillArray(tmp);
        for(int v : tmp){
            findSupp = false;
            for(int i : index){
                if(array[i].contains(v)){
                    zSupp.put(v,y.getSolver().getStateManager().makeStateInt(i));
                    findSupp = true;
                    break;
                }
            }
            if(!findSupp) z.remove(v);
        }
    }

    @Override
    public void post() {
        for(IntVar t : array) t.propagateOnBoundChange(this);
        y.propagateOnDomainChange(this);
        z.propagateOnDomainChange(this);
        propagate();
    }

    @Override
    public void propagate() {
        if(!y.isBound()) {
            // array support
            int[] index = new int[y.size()];
            y.fillArray(index);
            boolean indexChanged = false;
            for (int i : index) {
                if (!( array[i].contains(tSupp[i].value()) && z.contains(tSupp[i].value()) )) {
                    int[] tmp = new int[array[i].size()];
                    array[i].fillArray(tmp);
                    for (int v : tmp) {
                        if (z.contains(v)) {
                            tSupp[i].setValue(v);
                            break;
                        }
                    }
                    if (!( array[i].contains(tSupp[i].value()) && z.contains(tSupp[i].value()) )) {
                        y.remove(i);
                        indexChanged = true;
                    }
                }
            }
            if(indexChanged){
                index = new int[y.size()];
                y.fillArray(index);
            }
            // z support
            int[] values = new int[z.size()];
            z.fillArray(values);
            for(int v : values){
                StateInt zz = zSupp.get(v);
                if(!y.contains(zz.value())){
                    for(int i : index) {
                        if(array[i].contains(v)){
                            zz.setValue(i);
                            break;
                        }
                    }
                    if(!y.contains(zz.value())) z.remove(v);
                }
            }
        }
        else{
            y.getSolver().post(new Equal(array[y.min()],z));
            setActive(false);
        }
        /*int[] ySupp = new int[y.size()];
        int[] zSupp = new int[z.size()];
        int[] zSuppC = new int[z.size()];
        y.fillArray(ySupp);
        z.fillArray(zSupp);

        for(int index : ySupp){
            IntVar t = array[index];
            boolean belong = false;
            for(int i = 0; i < zSupp.length; i++) {
                if (t.contains(zSupp[i])) {
                    zSuppC[i]++;
                    belong = true;
                }
            }
            if(!belong) y.remove(index);
        }
        for(int i = 0; i < zSupp.length; i++){
            if(zSuppC[i] == 0) z.remove(zSupp[i]);
        }
        if(y.isBound()){
            IntVar t = array[y.max()];
            int[] tSupp = new int[t.size()];
            t.fillArray(tSupp);
            for(int i : tSupp){
                if(!z.contains(i)) t.remove(i);
            }
        }*/
    }

}
