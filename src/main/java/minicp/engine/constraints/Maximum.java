/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */


package minicp.engine.constraints;

import minicp.engine.core.AbstractConstraint;
import minicp.engine.core.IntVar;
import minicp.util.exception.InconsistencyException;
import minicp.util.exception.NotImplementedException;

/**
 * Maximum Constraint
 */
public class Maximum extends AbstractConstraint {

    private final IntVar[] x;
    private final IntVar y;

    /**
     * Creates the maximum constraint y = maximum(x[0],x[1],...,x[n])?
     *
     * @param x the variable on which the maximum is to be found
     * @param y the variable that is equal to the maximum on x
     */
    public Maximum(IntVar[] x, IntVar y) {
        super(x[0].getSolver());
        assert (x.length > 0);
        this.x = x;
        this.y = y;
    }


    @Override
    public void post() {
        y.propagateOnBoundChange(this);
        for(IntVar i : x){
            i.propagateOnBoundChange(this);
        }
        propagate();
    }


    @Override
    public void propagate() {
        IntVar VarMax = null;
        int nbVarMax = 0;
        for(int i = 0; i < x.length; i++) {
            if (x[i].max() > y.max()) x[i].removeAbove(y.max()); // Xs can't be greater then y
            if(x[i].max() >= y.min()) {                 // looking for candidates for x = max(y)
                nbVarMax++;
                VarMax = x[i];
            }
        }
        if(nbVarMax == 0) throw new InconsistencyException();
        else if(nbVarMax == 1) VarMax.removeBelow(y.min()); // only one candidate was found
        else {                                         // if multiple candidates were found -> update y
            int xmax = x[0].max();
            int xmin = x[0].min();
            for (int i = 1; i < x.length; i++) {
                if (x[i].max() > xmax) xmax = x[i].max();
                if (x[i].min() > xmin) xmin = x[i].min();
            }
            if (xmax < y.max()) y.removeAbove(xmax);
            if (xmin > y.min()) y.removeBelow(xmin);
        }
    }
}
