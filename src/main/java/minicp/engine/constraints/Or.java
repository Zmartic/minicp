/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */

package minicp.engine.constraints;

import minicp.engine.core.AbstractConstraint;
import minicp.engine.core.BoolVar;
import minicp.state.StateInt;
import minicp.util.exception.InconsistencyException;
import minicp.util.exception.NotImplementedException;

import static minicp.util.exception.InconsistencyException.INCONSISTENCY;

/**
 * Logical or constraint {@code  x1 or x2 or ... xn}
 */
public class Or extends AbstractConstraint { // x1 or x2 or ... xn

    private final BoolVar[] x;
    private final int n;
    private StateInt wL; // watched literal left
    private StateInt wR; // watched literal right


    /**
     * Creates a logical or constraint: at least one variable is true:
     * {@code  x1 or x2 or ... xn}
     *
     * @param x the variables in the scope of the constraint
     */
    public Or(BoolVar[] x) {
        super(x[0].getSolver());
        this.x = x;
        this.n = x.length;
        wL = getSolver().getStateManager().makeStateInt(0);
        wR = getSolver().getStateManager().makeStateInt(n - 1);
    }

    @Override
    public void post() {
        x[wL.value()].propagateOnBind(this);
        x[wR.value()].propagateOnBind(this);
        propagate();
    }


    @Override
    public void propagate() {
        // update watched literals

        // update wL
        int i = wL.value();
        while(x[i].isBound()) {
            if (x[i].min() == 1) {
                setActive(false);
                return;
            }
            else if(i == wR.value()) { // x[i] = 0
                throw new InconsistencyException();
            }
            i += 1;
        }
        if(wL.value() < i) {
            wL.setValue(i);
            x[wL.value()].propagateOnBind(this);
        }

        // update wR
        i = wR.value();
        while(x[i].isBound()) {
            if (x[i].min() == 1) {
                setActive(false);
                return;
            }
            /*else if(i == wR.value()) { // x[i] = 0
                throw new InconsistencyException(); //actually never reached
            }*/
            i -= 1;
        }
        if(wR.value() > i) {
            wR.setValue(i);
            x[wR.value()].propagateOnBind(this);
        }

        // wL == wR are unbound
        if (wL.value().equals(wR.value())) {
            x[wL.value()].assign(1);
            setActive(false);
        }
    }
}
