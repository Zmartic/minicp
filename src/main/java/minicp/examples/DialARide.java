package minicp.examples;

import minicp.engine.constraints.*;
import minicp.engine.core.BoolVar;
import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.search.DFSearch;
import minicp.search.Objective;
import minicp.search.SearchStatistics;
import minicp.util.io.InputReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.stream.IntStream;

import static minicp.cp.BranchingScheme.*;
import static minicp.cp.Factory.*;
import static minicp.cp.Factory.makeDfs;

public class DialARide {


    public static DialARideSolution solve(int nVehicles, int maxRouteDuration, int vehicleCapacity,
                                          int maxRideTime, ArrayList<RideStop> pickupRideStops, ArrayList<RideStop> dropRideStops,
                                          RideStop depot) {

        int NPick = pickupRideStops.size();
        int NDrop = dropRideStops.size();
        int n = nVehicles + NPick + NDrop;

        int[][] distanceMatrix = new int[n][n];
        // compute the upper triangle
        // * pick up lines
        for(int i = 0; i < NPick; i++){
            for(int j = i+1; j < NPick; j++){
                distanceMatrix[nVehicles + i][nVehicles + j] = distance(pickupRideStops.get(i),pickupRideStops.get(j));
            }
            for(int j = 0; j < NDrop; j++){
                distanceMatrix[nVehicles + i][nVehicles + NPick + j] = distance(pickupRideStops.get(i),dropRideStops.get(j));
            }
        }
        // * drop lines
        for(int i = 0; i < NDrop; i++){
            for(int j = i+1; j < NDrop; j++){
                distanceMatrix[nVehicles + NPick + i][nVehicles + NPick + j] = distance(dropRideStops.get(i),dropRideStops.get(j));
            }
        }
        // * depot lines
        for(int j = 0; j < NPick; j++){
            int tmp = distance(depot,pickupRideStops.get(j));
            for(int i = 0; i < nVehicles; i++) {
                distanceMatrix[i][nVehicles + j] = tmp;
            }
        }
        for(int j = 0; j < NDrop; j++){
            int tmp = distance(depot,dropRideStops.get(j));
            for(int i = 0; i < nVehicles; i++) {
                distanceMatrix[i][nVehicles + NPick + j] = tmp;
            }
        }
        // compute the lower triangle
        int maxDist = 0;
        for(int i = 0; i < n; i++){
            for(int j = i+1; j < n; j++){
                distanceMatrix[j][i] = distanceMatrix[i][j];
                if (distanceMatrix[i][j] > maxDist) {
                    maxDist = distanceMatrix[i][j];
                }
            }
        }



        /// --- Constrains definition ---

        Solver cp = makeSolver(false);
        BoolVar IsTrue = makeBoolVar(cp);
        IsTrue.assign(true);
        BoolVar IsFalse = makeBoolVar(cp);
        IsFalse.assign(false);

        IntVar[] succ = makeIntVarArray(cp, n, n);                    // Successor   of node i
        IntVar[] pred = makeIntVarArray(cp, n, n);                    // Predecessor of node i
        IntVar[] distSucc = makeIntVarArray(cp, n, maxDist); // Distance between node i and its successor
        IntVar[] distPred = makeIntVarArray(cp, n, maxDist); // Distance between node i and its successor
        IntVar[] time_Reached = new IntVar[n];                        // Time when node i is visited
        for(int i = 0; i < nVehicles; i++) time_Reached[i]                 = makeIntVar(cp,0,maxRouteDuration);
        for(int i = 0; i < NPick; i++) time_Reached[nVehicles + i]         = makeIntVar(cp,distanceMatrix[nVehicles + i][0],pickupRideStops.get(i).window_end);
        for(int i = 0; i < NDrop; i++) time_Reached[nVehicles + NPick + i] = makeIntVar(cp,distanceMatrix[nVehicles + NPick + i][0],dropRideStops.get(i).window_end);
        IntVar[] who_Reached = makeIntVarArray(cp, n, nVehicles);     // Vehicle how reached node i
        IntVar[] size = makeIntVarArray(cp, n, vehicleCapacity);      // Number of people in the vehicle when arriving at a stop


        // * Prevent vehicles to move from depot -> depot
        for(int i = 0; i < nVehicles; i++){
            for(int j = 0; j < nVehicles; j++) {
                succ[i].remove(j);
                pred[j].remove(i);
            }
        }
        // remove all drop points from the domain of succ[depot]
//        for (int i = 0; i < nVehicles; i++) {
//            for (int j = nVehicles + NPick; j < n; j++){
//                succ[i].remove(j);
//            }
//        }

        // * define distSucc
        for (int i = 0; i < n; i++) {
            // distanceMatrix[ i ][ succ[i] ] = distSucc[ i ]
            cp.post(new Element1D(distanceMatrix[i], succ[i], distSucc[i]));
        }
        // * define distPred
//        for (int i = 0; i < n; i++) {
//            // distanceMatrix[ i ][ succ[i] ] = distSucc[ i ]
//            cp.post(new Element1D(distanceMatrix[i], pred[i], distPred[i]));
//        }

        // * define time_Reached
        for(int i = 0; i < nVehicles; i++) {
            // time_Reached[ succ[i] ] = distSucc[ i ] + 0   (i = depot)
            cp.post(new Element1DVar(time_Reached, succ[i], distSucc[i]));
        }
        for(int i = nVehicles; i < n; i++) {
            // time_Reached[ succ[i] ] = distSucc[ i ] + time_Reached[ i ]   (i != depot)
            cp.post(new Element1DVar(time_Reached, succ[i], sum(time_Reached[i],distSucc[i])));

            // time_Reached[ i ] + distance[ i ][ depot ] =< maxRouteDuration
            int time = maxRouteDuration - distanceMatrix[i][0] ;
            cp.post(new LessOrEqual(time_Reached[i],makeIntVar(cp, time, time)));
        }

        // * define who_Reached
        for(int i = 0; i < nVehicles; i++){
            // each vehicles start at the depot
            cp.post(new Element1DVar(who_Reached,succ[i],makeIntVar(cp, i, i)));
        }
        for(int i = nVehicles; i < n; i++){
            // who_Reached[ succ[i] ] = who_Reached[ i ]   (i != depot)
            cp.post(new Element1DVar(who_Reached,succ[i],who_Reached[i]));
        }

        // * define size
        for(int i = 0; i < nVehicles; i++) {
            // nobody should be at the depot
            size[i].assign(0);
        }
        for(int i = 0; i < nVehicles; i++) {
            // leave the depot : size[ succ[i] ] = size[i] + 0
            cp.post(new Element1DVar(size, succ[i], plus(size[i],0)));
        }
        for(int i = nVehicles; i < nVehicles + NPick; i++) {
            // pick someone    : size[ succ[i] ] = size[i] + 1
            cp.post(new Element1DVar(size, succ[i], plus(size[i],1)));
            size[i].remove(vehicleCapacity);
        }
        for(int i = nVehicles + NPick; i < n; i++) {
            // drop someone    : size[ succ[i] ] = size[i] - 1
            cp.post(new Element1DVar(size, succ[i], plus(size[i],-1)));
            size[i].remove(0);
        }

        // * Pick and Drop constrain
        for(int i = 0; i < NPick; i++){
            int pick = nVehicles + i;
            int drop = nVehicles + NPick + i;
            int dist = distanceMatrix[pick][drop];
            // Associated pick and drop stop have to be visited by the same vehicle
            cp.post(new Equal(who_Reached[pick],who_Reached[drop]));
            // People must be picked before being dropped    : t_start + dist <= t_end
            cp.post(new IsLessOrEqualVar(IsTrue,plus(time_Reached[pick],dist),time_Reached[drop]));
            // People can remain in the vehicle for too long : t_end - t_start <= maxRideTime
            cp.post(new IsLessOrEqualVar(IsTrue,time_Reached[drop],plus(time_Reached[pick],maxRideTime)));
            // Guide vers le drop
            /*IntVar time_succ = makeIntVar(cp, 0, pickupRideStops.get(i).window_end);
            IntVar dist_succ = makeIntVar(cp, 0, maxRouteDuration);
            cp.post(new Element1DVar(time_Reached,succ[pick],time_succ));
            cp.post(new Element1D(distanceMatrix[drop],succ[pick],dist_succ));
            cp.post(new IsLessOrEqualVar(IsTrue,sum(time_succ,dist_succ),time_Reached[drop]));
            cp.post(new IsLessOrEqual(IsTrue,sum(distSucc[pick],dist_succ),maxRideTime));*/
        }

        // * Hamiltonien Circuit
        cp.post(new Circuit(succ));

        // * define pred
        for(int i = 0; i < n; i++){
            // succ[ pred[i] ] = i
            cp.post(new Element1DVar(succ,pred[i],makeIntVar(cp, i, i)));
            pred[i].remove(i);
        }
        //cp.post(new AllDifferentDC(pred));

        // ** Objectif
        IntVar totalDist = sum(distSucc);

        /// --- DFS search with heuristic ---

        Objective obj = cp.minimize(totalDist);

        Integer[] DFS_heuristic = new Integer[n];
        for(int i = 0; i < n; i++) DFS_heuristic[i] = i;
        DFSearch dfs = makeDfs(cp, () -> {
            // ** select the most important node to visit
            Integer xi = selectMin(DFS_heuristic,
                    i -> pred[i].size() > 1,
                    i -> time_Reached[i].max()
                    );
            if (xi == null)
                return EMPTY;
            else {
                IntVar xs = pred[xi];
                int[] vs = new int[xs.size()];
                xs.fillArray(vs);
                // select the vehicle with the minimum traveling time
                Integer v = selectMin(IntStream.of(vs).boxed().toArray(Integer[]::new),
                        i -> true,
                        i -> { if (i < nVehicles) return distanceMatrix[xi][i];
                               else               return time_Reached[i].max() + distanceMatrix[xi][i];
                        });
                return branch(() -> xs.getSolver().post(equal(xs, v)),
                              () -> xs.getSolver().post(notEqual(xs, v)));
                /*return branch(() -> {System.out.println("on :"+xi+" -> "+v+" | "+pred[xi]);xs.getSolver().post(equal(xs, v));},
                              () -> {System.out.println("off :"+xi+" -> "+v +" | "+pred[xi]);xs.getSolver().post(notEqual(xs, v));});*/
            }
        });

        // --- Large Neighborhood Search ---

        // Current best solution
        int[] succBest = IntStream.range(0, n).toArray();
        double[] totalDistBest = new double[1];

        dfs.onSolution(() -> {
            // Update the current best solution
            for (int i = 0; i < n; i++) {
                succBest[i] = succ[i].min();
            }
            totalDistBest[0] = totalDist.min();
            System.out.println("objective:" + totalDist.min());
        });

        // LNS parameters

        int nRestarts = 150;
        int failureLimit = 85;
        double pcUnchanged = 0.6;


        dfs.optimizeSubjectTo(obj, statistics -> statistics.numberOfFailures() >= failureLimit, () -> {

                }
        );


        Random rand = new java.util.Random(0);

        for (int i = 0; i < nRestarts; i++) {
            if (i % 10 == 0)
                System.out.println("restart number #" + i);
            dfs.optimizeSubjectTo(obj, statistics -> statistics.numberOfFailures() >= failureLimit, () -> {
                        // Assign the fragment % of the variables randomly chosen
                        for (int j = 1; j < n; j++) {
                            double weight = (double) n * (double) distanceMatrix[j][succBest[j]] / totalDistBest[0];
                            if (weight*rand.nextDouble() < pcUnchanged) {
                                cp.post(equal(succ[j], succBest[j]));
                            }
                        }
                    }
            );
        }


        //SearchStatistics stats = dfs.optimize(obj, statistics -> statistics.numberOfSolutions() == 1);
        //System.out.println(stats);

        /// --- Generate the final solution ---

        DialARideSolution solution = new DialARideSolution(nVehicles, pickupRideStops, dropRideStops,
                                                       depot, vehicleCapacity, maxRideTime, maxRouteDuration);

        for (int i = 0; i < nVehicles; i++){
            int current = i;
            while(succBest[current] >= nVehicles){
                if(succBest[current] < nVehicles + NPick) {
                    solution.addStop(i,succBest[current]-nVehicles,true);
                }
                else{
                    solution.addStop(i,succBest[current]-nVehicles-NPick,false);
                }
                current = succBest[current];
            }
        }

        return solution;
    }

    /**
     * Returns the distance between two ride stops
     */
    public static int distance(RideStop a, RideStop b) {
        return (int) (Math.sqrt((a.pos_x - b.pos_x) * (a.pos_x - b.pos_x) + (a.pos_y - b.pos_y) * (a.pos_y - b.pos_y)) * 100);
    }

    /**
     * A solution. To create one, first do new DialARideSolution, then
     * add, for each vehicle, in order, the pickup/drops with addStop(vehicleIdx, rideIdx, isPickup), where
     * vehicleIdx is an integer beginning at 0 and ending at nVehicles - 1, rideIdx is the id of the ride you (partly)
     * fullfill with this stop (from 0 to pickupRideStops.size()-1) and isPickup a boolean indicate if you are beginning
     * or ending the ride. Do not add the last stop to the depot, it is implicit.
     * <p>
     * You can check the validity of your solution with compute(), which returns the total distance, and raises an
     * exception if something is invalid.
     * <p>
     * DO NOT MODIFY THIS CLASS.
     */
    public static class DialARideSolution {
        public ArrayList<Integer>[] stops;
        public ArrayList<RideStop> pickupRideStops;
        public ArrayList<RideStop> dropRideStops;
        public RideStop depot;
        public int capacity;
        public int maxRideTime;
        public int maxRouteDuration;

        public String toString() {
            StringBuilder b = new StringBuilder();
            b.append("Length: ");
            b.append(compute());
            b.append("\n");
            for (int i = 0; i < stops.length; i++) {
                b.append("- ");
                for (int s : stops[i]) {
                    if (s >= pickupRideStops.size()) {
                        b.append(s - pickupRideStops.size());
                        b.append("d, ");
                    } else {
                        b.append(s);
                        b.append("p, ");
                    }
                }
                b.append("\n");
            }
            return b.toString();
        }

        public DialARideSolution(int nVehicles, ArrayList<RideStop> pickupRideStops, ArrayList<RideStop> dropRideStops,
                                 RideStop depot, int vehicleCapacity, int maxRideTime, int maxRouteDuration) {
            stops = new ArrayList[nVehicles];
            for (int i = 0; i < nVehicles; i++)
                stops[i] = new ArrayList<>();

            this.pickupRideStops = pickupRideStops;
            this.dropRideStops = dropRideStops;
            this.depot = depot;
            this.capacity = vehicleCapacity;
            this.maxRideTime = maxRideTime;
            this.maxRouteDuration = maxRouteDuration;
        }

        public void addStop(int vehicleId, int rideId, boolean isPickup) {
            stops[vehicleId].add(rideId + (isPickup ? 0 : pickupRideStops.size()));
        }

        public int compute() {
            int totalLength = 0;
            HashSet<Integer> seenRides = new HashSet<>();

            for (int vehicleId = 0; vehicleId < stops.length; vehicleId++) {
                HashMap<Integer, Integer> inside = new HashMap<>();
                RideStop current = depot;
                int currentLength = 0;
                for (int next : stops[vehicleId]) {
                    RideStop nextStop;
                    if (next < pickupRideStops.size())
                        nextStop = pickupRideStops.get(next);
                    else
                        nextStop = dropRideStops.get(next - pickupRideStops.size());

                    currentLength += distance(current, nextStop);

                    if (next < pickupRideStops.size()) {
                        if (seenRides.contains(next))
                            throw new RuntimeException("Ride stop visited twice");
                        seenRides.add(next);
                        inside.put(next, currentLength);
                    } else {
                        if (!inside.containsKey(next - pickupRideStops.size()))
                            throw new RuntimeException("Drop before pickup");
                        if (inside.get(next - pickupRideStops.size()) + maxRideTime < currentLength)
                            throw new RuntimeException("Ride time too long");
                        inside.remove(next - pickupRideStops.size());
                    }

                    if (currentLength > nextStop.window_end)
                        throw new RuntimeException("Ride stop visited too late");
                    if (inside.size() > capacity)
                        throw new RuntimeException("Above maximum capacity");

                    current = nextStop;
                }

                currentLength += distance(current, depot);

                if (inside.size() > 0)
                    throw new RuntimeException("Passenger never dropped");
                if (currentLength > maxRouteDuration)
                    throw new RuntimeException("Route too long");

                totalLength += currentLength;
            }

            if (seenRides.size() != pickupRideStops.size())
                throw new RuntimeException("Some rides never fulfilled");

            return totalLength;
        }
    }

    static class RideStop {
        public float pos_x;
        public float pos_y;
        public int type; //0 == depot, 1 == pickup, -1 == drop
        public int window_end;
    }

    public static RideStop readRide(InputReader reader) {
        try {
            RideStop r = new RideStop();
            reader.getInt(); //ignored
            r.pos_x = Float.parseFloat(reader.getString());
            r.pos_y = Float.parseFloat(reader.getString());
            reader.getInt(); //ignored
            r.type = reader.getInt();
            reader.getInt(); //ignored
            r.window_end = reader.getInt() * 100;
            return r;
        } catch (Exception e) {
            return null;
        }
    }

    public static void main(String[] args) {
        // Reading the data

        //TODO change file to test the various instances.
        InputReader reader = new InputReader("data/dialaride/custom2");

        int nVehicles = reader.getInt();
        reader.getInt(); //ignore
        int maxRouteDuration = reader.getInt() * 100;
        int vehicleCapacity = reader.getInt();
        int maxRideTime = reader.getInt() * 100;

        RideStop depot = null;
        ArrayList<RideStop> pickupRideStops = new ArrayList<>();
        ArrayList<RideStop> dropRideStops = new ArrayList<>();
        boolean lastWasNotDrop = true;
        while (true) {
            RideStop r = readRide(reader);
            if (r == null)
                break;
            if (r.type == 0) {
                assert depot == null;
                depot = r;
            } else if (r.type == 1) {
                assert lastWasNotDrop;
                pickupRideStops.add(r);
            } else { //r.type == -1
                lastWasNotDrop = false;
                dropRideStops.add(r);
            }
        }
        assert depot != null;
        assert pickupRideStops.size() == dropRideStops.size();

        DialARideSolution sol = solve(nVehicles, maxRouteDuration, vehicleCapacity, maxRideTime, pickupRideStops, dropRideStops, depot);
        System.out.println(sol);
    }
}
