package minicp.examples;

import minicp.engine.constraints.Circuit;
import minicp.engine.constraints.Element1D;
import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.search.DFSearch;
import minicp.search.Objective;
import minicp.search.SearchStatistics;
import minicp.util.io.InputReader;

import java.util.Random;
import java.util.stream.IntStream;

import static minicp.cp.BranchingScheme.*;
import static minicp.cp.Factory.*;
import static minicp.cp.Factory.notEqual;

public class VRP {


    public static void main(String[] args) {


        // instance gr17 https://people.sc.fsu.edu/~jburkardt/datasets/tsp/gr17_d.txt
        InputReader reader = new InputReader("data/tsp.txt");


        int n = reader.getInt();
        int k = 3; // number of vehicles
        int m = n + k - 1;

        int[][] distanceMatrix_reader = reader.getMatrix(n, n);
        int[][] distanceMatrix = new int[m][m];

        for (int i = 0; i < k; i++) {
            for(int j = k; j < m; j++) {
                distanceMatrix[i][j] = distanceMatrix_reader[0][j-k+1];
                distanceMatrix[j][i] = distanceMatrix_reader[j-k+1][0];
            }
        }
        for (int i = k; i < m; i++) {
            for(int j = k; j < m; j++) {
                distanceMatrix[i][j] = distanceMatrix_reader[i-k+1][j-k+1];
            }
        }



        Solver cp = makeSolver(false);
        IntVar[] succ = makeIntVarArray(cp, m, m);
        IntVar[] distSucc = makeIntVarArray(cp, m, 1000);
        for(int i = 0; i < k; i++){
            succ[i].removeBelow(k);
        }

        cp.post(new Circuit(succ));

        for (int i = 0; i < m; i++) {
            cp.post(new Element1D(distanceMatrix[i], succ[i], distSucc[i]));
        }

        IntVar totalDist = sum(distSucc);

        Objective obj = cp.minimize(totalDist);

        DFSearch dfs = makeDfs(cp, () -> {
            IntVar xs = selectMin(succ,
                    xi -> xi.size() > 1,
                    xi -> xi.size());
            if (xs == null)
                return EMPTY;
            else {
                int v = xs.min();
                return branch(() -> xs.getSolver().post(equal(xs, v)),
                        () -> xs.getSolver().post(notEqual(xs, v)));
            }
        });

        int[] succBest = IntStream.range(0, m).toArray();

        dfs.onSolution(() -> {
            // Update the current best solution
            for (int i = 0; i < m; i++) {
                succBest[i] = succ[i].min();
            }
            System.out.println("objective:" + totalDist.min());
        });

        // LNS parameters
        int nRestarts = 100;
        int failureLimit = 100;
        double pcUnchanged = 0.05;


        Random rand = new java.util.Random(0);

        for (int i = 0; i < nRestarts; i++) {
            if (i % 10 == 0)
                System.out.println("restart number #" + i);
            dfs.optimizeSubjectTo(obj, statistics -> statistics.numberOfFailures() >= failureLimit, () -> {
                        // Assign the fragment % of the variables randomly chosen
                        for (int j = 0; j < m; j++) {
                            if (rand.nextDouble() < pcUnchanged) {
                                cp.post(equal(succ[j], succBest[j]));
                            }
                        }
                    }
            );
        }




    }
}
